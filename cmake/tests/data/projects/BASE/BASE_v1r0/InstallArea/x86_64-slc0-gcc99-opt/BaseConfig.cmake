#####################################################################################
# (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################
# File automatically generated: DO NOT EDIT.
set(Base_heptools_version 69)
set(Base_heptools_system x86_64-slc6-gcc48)

set(Base_PLATFORM x86_64-slc0-gcc99-opt)

set(Base_VERSION v1r0)
set(Base_VERSION_MAJOR 1)
set(Base_VERSION_MINOR 0)
set(Base_VERSION_PATCH )

set(Base_USES )

#list(INSERT CMAKE_MODULE_PATH 0 ${Base_DIR}/cmake)
#include(BasePlatformConfig)
